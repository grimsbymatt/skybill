﻿using Xunit;
using NSubstitute;
using SkyBilling.BillServiceApi;
using SkyBilling.Common.Models;
using SkyBilling.UnitTests.Common;
using SkyBilling.WebService.Controllers;

namespace SkyBilling.WebService.UnitTests.Controllers
{
    public class BillControllerTests
    {
        [Fact]
        public void Get_HelperReturnsNull_NullReturned()
        {
            // Arrange
            var fakeBillService = Substitute.For<IBillServiceApi>();

            fakeBillService.GetBill().ReturnsForAnyArgs((SkyBillModel) null);

            var classUnderTest = new BillController(fakeBillService);

            // Act
            var result = classUnderTest.Get();

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void Get_HelperReturnsPopulatedModel_ModelReturnedUnchanged()
        {
            // Arrange
            var returnedModel = TestUtils.GetTestModel();

            var fakeBillService = Substitute.For<IBillServiceApi>();

            fakeBillService.GetBill().ReturnsForAnyArgs(returnedModel);

            var classUnderTest = new BillController(fakeBillService);

            // Act
            var result = classUnderTest.Get();

            // Assert
            Assert.NotNull(result);
            TestUtils.CompareModels(returnedModel, result);
        }
    }
}
