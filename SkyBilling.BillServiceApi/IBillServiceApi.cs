﻿using SkyBilling.Common.Models;

namespace SkyBilling.BillServiceApi
{
    /// <summary>
    /// Interface that provides access to the billing backend service.
    /// </summary>
    public interface IBillServiceApi
    {
        /// <summary>
        /// Requests a bill from the billing backend service.
        /// </summary>
        /// <returns>A <see cref="SkyBillModel"/> of the returned bill.</returns>
        SkyBillModel GetBill();
    }
}
