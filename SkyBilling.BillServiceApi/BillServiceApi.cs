﻿using System;
using SkyBilling.Common.Models;
using SkyBilling.Common.WebRequestHelper;

namespace SkyBilling.BillServiceApi
{
    /// <summary>
    /// Implementation of <see cref="IBillServiceApi"/> that
    /// consumes backend billing web service .
    /// </summary>
    internal class BillServiceApi : IBillServiceApi
    {
        private IWebRequestHelper WebRequestHelper { get; }

        private const string BillServiceResource = "http://safe-plains-5453.herokuapp.com/bill.json";

        /// <summary>
        /// Constructor that takes <see cref="IWebRequestHelper"/>
        /// dependency.
        /// </summary>
        /// <param name="webRequestHelper">The <see cref="IWebRequestHelper"/>
        /// dependency.</param>
        public BillServiceApi(IWebRequestHelper webRequestHelper)
        {
            if (webRequestHelper == null)
            {
                throw new ArgumentNullException(nameof(webRequestHelper));
            }

            this.WebRequestHelper = webRequestHelper;
        }

        /// <inheritdoc />
        public SkyBillModel GetBill()
        {
            return this.WebRequestHelper.GetJsonResponse<SkyBillModel>(BillServiceResource);
        }
    }
}
