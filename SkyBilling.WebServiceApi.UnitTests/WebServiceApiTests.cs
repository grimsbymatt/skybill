﻿using System;
using Xunit;
using NSubstitute;
using SkyBilling.Common.Models;
using SkyBilling.Common.WebRequestHelper;
using SkyBilling.UnitTests.Common;

namespace SkyBilling.WebServiceApi.UnitTests
{
    public class WebServiceApiTests
    {
        [Fact]
        public void WebServiceApi_PassNullToConstructor_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(
                () => new WebServiceApi(null));
        }

        [Fact]
        public void GetBill_HelperReturnsNull_HelperReceivesCallAndNullIsReturned()
        {
            // Arrange
            var fakeWebRequestHelper = Substitute.For<IWebRequestHelper>();

            fakeWebRequestHelper
                .GetJsonResponse<SkyBillModel>(Arg.Any<string>())
                .ReturnsForAnyArgs((SkyBillModel) null);

            var classUnderTest = new WebServiceApi(fakeWebRequestHelper);

            // Act
            var result = classUnderTest.GetBill();

            // Assert
            fakeWebRequestHelper
                .Received()
                .GetJsonResponse<SkyBillModel>("http://localhost:51708/api/Bill");

            Assert.Null(result);
        }

        [Fact]
        public void GetBill_HelperReturnsPopulatedModel_HelperReceivesCallAndModelReturnedUnchanged()
        {
            // Arrange
            var returnedModel = TestUtils.GetTestModel();

            var fakeWebRequestHelper = Substitute.For<IWebRequestHelper>();

            fakeWebRequestHelper
                .GetJsonResponse<SkyBillModel>(Arg.Any<string>())
                .ReturnsForAnyArgs(returnedModel);

            var classUnderTest = new WebServiceApi(fakeWebRequestHelper);

            // Act
            var result = classUnderTest.GetBill();

            // Assert
            fakeWebRequestHelper
                .Received()
                .GetJsonResponse<SkyBillModel>("http://localhost:51708/api/Bill");

            Assert.NotNull(result);

            TestUtils.CompareModels(returnedModel, result);
        }
    }
}
