﻿using Ninject;
using SkyBilling.Website.Dependencies;
using Xunit;

namespace SkyBilling.WebServiceApi.IntegrationTests
{
    public class WebServiceApiIntegrationTests
    {
        [Fact]
        public void GetBill_CallService_ModelReceivedCorrectly()
        {
            // Arrange
            var webService = GetWebService();

            // Act
            var result = webService.GetBill();

            // Assert
            Assert.NotNull(result);
        }

        #region HelperMethods

        private static IWebServiceApi GetWebService()
        {
            var kernel = new StandardKernel();
            kernel.Load(new WebsiteModule());
            return kernel.Get<IWebServiceApi>();
        }

        #endregion
    }
}
