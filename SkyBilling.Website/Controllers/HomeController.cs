﻿using System.Web.Mvc;
using Ninject;
using SkyBilling.Website.Dependencies;
using SkyBilling.WebServiceApi;

namespace SkyBilling.Website.Controllers
{
    /// <summary>
    /// Controller that handles website billing requests.
    /// </summary>
    public class HomeController : Controller
    {
        private IWebServiceApi WebServiceApi { get; }

        public HomeController()
        {
            var kernel = new StandardKernel();
            kernel.Load(new WebsiteModule());
            this.WebServiceApi = kernel.Get<IWebServiceApi>();
        }

        /// <summary>
        /// Internal constructor for testing that takes 
        /// <see cref="IWebServiceApi"/> dependency.
        /// </summary>
        /// <param name="webServiceApi">Implementation of 
        /// <see cref="IWebServiceApi"/>.</param>
        internal HomeController(IWebServiceApi webServiceApi)
        {
            this.WebServiceApi = webServiceApi;
        }

        public ActionResult Index()
        {
            var model = this.WebServiceApi.GetBill();
            return View(model);
        }
    }
}