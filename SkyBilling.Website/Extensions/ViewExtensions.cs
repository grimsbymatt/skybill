﻿using System;
using System.Globalization;

namespace SkyBilling.Website.Extensions
{
    /// <summary>
    /// Extension methods to format values for bill view.
    /// </summary>
    public static class ViewExtensions
    {
        private const string DateTimeFormat = "dd MMMM yyyy";

        private const string CurrencyCulture = "en-GB";

        /// <summary>
        /// Converts a <see cref="DateTime"/> to formatted <see cref="string"/>.
        /// </summary>
        /// <param name="dateTime">The <see cref="DateTime"/>
        /// to convert.</param>
        /// <returns>A formatted <see cref="string"/> of the value.</returns>
        public static string ToViewFormat(this DateTime dateTime)
        {
            return dateTime.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a <see cref="decimal"/> to formatted currency
        /// <see cref="string"/>.
        /// </summary>
        /// <param name="value">The <see cref="decimal"/> value to
        /// convert.</param>
        /// <returns>A formatted <see cref="string"/> of the value.</returns>
        public static string ToViewCurrencyFormat(this decimal value)
        {
            return value.ToString("C", CultureInfo.GetCultureInfo(CurrencyCulture));
        }
    }
}