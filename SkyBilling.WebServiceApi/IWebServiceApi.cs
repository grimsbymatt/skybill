﻿using SkyBilling.Common.Models;

namespace SkyBilling.WebServiceApi
{
    /// <summary>
    /// Interface that provides access to the billing web service.
    /// </summary>
    public interface IWebServiceApi
    {
        /// <summary>
        /// Get a bill.
        /// </summary>
        /// <returns>A <see cref="SkyBillModel"/>.</returns>
        SkyBillModel GetBill();
    }
}
