﻿using System;
using SkyBilling.Common.Models;
using SkyBilling.Common.WebRequestHelper;

namespace SkyBilling.WebServiceApi
{
    /// <summary>
    /// Implementation of <see cref="IWebServiceApi"/> that consumes
    /// business layer billing web service.
    /// </summary>
    internal class WebServiceApi : IWebServiceApi
    {
        private IWebRequestHelper WebRequestHelper { get; }

        private const string WebServiceResource = "http://localhost:51708/api/Bill";

        /// <summary>
        /// Constructor that takes <see cref="IWebRequestHelper"/>
        /// dependency.
        /// </summary>
        /// <param name="webRequestHelper"></param>
        public WebServiceApi(IWebRequestHelper webRequestHelper)
        {
            if (webRequestHelper == null)
            {
                throw new ArgumentNullException();
            }

            this.WebRequestHelper = webRequestHelper;
        }

        /// <inheritdoc />
        public SkyBillModel GetBill()
        {
            return this.WebRequestHelper.GetJsonResponse<SkyBillModel>(WebServiceResource);
        }
    }
}