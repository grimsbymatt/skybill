﻿using Ninject.Modules;
using SkyBilling.Common.WebRequestHelper;
using SkyBilling.WebServiceApi;

namespace SkyBilling.Website.Dependencies
{
    public class WebsiteModule : NinjectModule
    {
        public override void Load()
        {
            // SkyBilling.Common
            Bind<IWebRequestHelper>().To<WebRequestHelper>();

            // SkyBilling.WebServiceApi
            Bind<IWebServiceApi>().To<WebServiceApi.WebServiceApi>();
        }
    }
}
