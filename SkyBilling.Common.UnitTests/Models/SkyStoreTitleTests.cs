﻿using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class SkyStoreTitleTests
    {
        [Fact]
        public void Title_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new SkyStoreTitle
            {
                Title = "TitleTest"
            };

            // Act
            var result = classUnderTest.Title;

            // Assert
            Assert.Equal("TitleTest", result);
        }

        [Fact]
        public void Cost_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new SkyStoreTitle
            {
                Cost = 1.23m
            };

            // Act
            var result = classUnderTest.Cost;

            // Assert
            Assert.Equal(1.23m, result);
        }
    }
}
