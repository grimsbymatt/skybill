﻿using System.Collections.Generic;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class SkyStoreTests
    {
        [Fact]
        public void Rentals_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testTitles = new List<SkyStoreTitle>
            {
                new SkyStoreTitle
                {
                    Title = "TestTitle1",
                    Cost = 1.23m
                },
                new SkyStoreTitle
                {
                    Title = "TestTitle2",
                    Cost = 4.56m
                }
            };

            var classUnderTest = new SkyStore
            {
                Rentals = testTitles
            };

            // Act
            var result = classUnderTest.Rentals;

            // Assert
            Assert.Equal(2, result.Count);

            Assert.Equal("TestTitle1", result[0].Title);
            Assert.Equal(1.23m, result[0].Cost);

            Assert.Equal("TestTitle2", result[1].Title);
            Assert.Equal(4.56m, result[1].Cost);
        }

        [Fact]
        public void BuyAndKeep_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testTitles = new List<SkyStoreTitle>
            {
                new SkyStoreTitle
                {
                    Title = "TestTitle1",
                    Cost = 1.23m
                },
                new SkyStoreTitle
                {
                    Title = "TestTitle2",
                    Cost = 4.56m
                }
            };

            var classUnderTest = new SkyStore
            {
                BuyAndKeep = testTitles
            };

            // Act
            var result = classUnderTest.BuyAndKeep;

            // Assert
            Assert.Equal(2, result.Count);

            Assert.Equal("TestTitle1", result[0].Title);
            Assert.Equal(1.23m, result[0].Cost);

            Assert.Equal("TestTitle2", result[1].Title);
            Assert.Equal(4.56m, result[1].Cost);
        }

        [Fact]
        public void Total_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new SkyStore
            {
                Total = 123.45m
            };

            // Act
            var result = classUnderTest.Total;

            // Assert
            Assert.Equal(123.45m, result);
        }
    }
}
