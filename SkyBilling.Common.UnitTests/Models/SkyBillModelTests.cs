﻿using System;
using System.Collections.Generic;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class SkyBillModelTests
    {
        [Fact]
        public void Statement_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testFromDateTime = DateTime.Parse("2016-04-13");
            var testToDateTime = DateTime.Parse("2016-05-12");
            var testGeneratedDateTime = DateTime.Parse("2016-05-14");
            var testDueDateTime = DateTime.Parse("2016-06-01");

            var testPeriod = new Period
            {
                From = testFromDateTime,
                To = testToDateTime
            };

            var testStatement = new Statement
            {
                Generated = testGeneratedDateTime,
                Due = testDueDateTime,
                Period = testPeriod
            };

            var classUnderTest = new SkyBillModel
            {
                Statement = testStatement
            };

            // Act
            var result = classUnderTest.Statement;

            // Assert
            Assert.Equal(testGeneratedDateTime, result.Generated);
            Assert.Equal(testDueDateTime, result.Due);
            Assert.Equal(testFromDateTime, result.Period.From);
            Assert.Equal(testToDateTime, result.Period.To);
        }

        [Fact]
        public void Total_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new SkyBillModel
            {
                Total = 123.45m
            };

            // Act
            var result = classUnderTest.Total;

            // Assert
            Assert.Equal(123.45m, result);
        }

        [Fact]
        public void Package_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testSubscriptions = new List<Subscription>
            {
                new Subscription
                {
                    Name = "Subscription1",
                    Cost = 1.23m,
                    Type = SubscriptionType.Tv
                },
                new Subscription
                {
                    Name = "Subscription2",
                    Cost = 4.56m,
                    Type = SubscriptionType.Broadband
                },
                new Subscription
                {
                    Name = "Subscription3",
                    Cost = 7.89m,
                    Type = SubscriptionType.Talk
                }
            };

            var testPackage = new Package
            {
                Subscriptions = testSubscriptions,
                Total = 123.45m
            };

            var classUnderTest = new SkyBillModel
            {
                Package = testPackage
            };

            // Act
            var result = classUnderTest.Package;

            // Assert
            Assert.Equal(123.45m, result.Total);

            Assert.Equal(3, result.Subscriptions.Count);

            Assert.Equal("Subscription1", result.Subscriptions[0].Name);
            Assert.Equal(1.23m, result.Subscriptions[0].Cost);
            Assert.Equal(SubscriptionType.Tv, result.Subscriptions[0].Type);

            Assert.Equal("Subscription2", result.Subscriptions[1].Name);
            Assert.Equal(4.56m, result.Subscriptions[1].Cost);
            Assert.Equal(SubscriptionType.Broadband, result.Subscriptions[1].Type);

            Assert.Equal("Subscription3", result.Subscriptions[2].Name);
            Assert.Equal(7.89m, result.Subscriptions[2].Cost);
            Assert.Equal(SubscriptionType.Talk, result.Subscriptions[2].Type);
        }

        [Fact]
        public void CallCharges_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testTimeSpan = TimeSpan.ParseExact("11.35", "mm'.'ss", null);

            var testCalls = new List<Call>
            {
                new Call
                {
                    Called = "0123456789",
                    Cost = 1.23m,
                    Duration = testTimeSpan
                },
                new Call
                {
                    Called = "0987654321",
                    Cost = 4.56m,
                    Duration = testTimeSpan
                }
            };

            var testCallCharges = new CallCharges
            {
                Calls = testCalls,
                Total = 5.79m
            };

            var classUnderTest = new SkyBillModel
            {
                CallCharges = testCallCharges
            };

            // Act
            var result = classUnderTest.CallCharges;

            // Assert
            Assert.Equal(5.79m, result.Total);

            Assert.Equal(2, result.Calls.Count);

            Assert.Equal("0123456789", result.Calls[0].Called);
            Assert.Equal(1.23m, result.Calls[0].Cost);
            Assert.Equal(testTimeSpan, result.Calls[0].Duration);

            Assert.Equal("0987654321", result.Calls[1].Called);
            Assert.Equal(4.56m, result.Calls[1].Cost);
            Assert.Equal(testTimeSpan, result.Calls[1].Duration);
        }

        [Fact]
        public void SkyStore_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testRentalTitles = new List<SkyStoreTitle>
            {
                new SkyStoreTitle
                {
                    Title = "TestTitle1",
                    Cost = 1.23m
                },
                new SkyStoreTitle
                {
                    Title = "TestTitle2",
                    Cost = 4.56m
                }
            };

            var testBuyAndKeepTitles = new List<SkyStoreTitle>
            {
                new SkyStoreTitle
                {
                    Title = "TestTitle3",
                    Cost = 7.89m
                }
            };

            var testSkyStore = new SkyStore
            {
                Total = 13.68m,
                Rentals = testRentalTitles,
                BuyAndKeep = testBuyAndKeepTitles
            };

            var classUnderTest = new SkyBillModel
            {
                SkyStore = testSkyStore
            };

            // Act
            var result = classUnderTest.SkyStore;

            // Assert
            Assert.Equal(13.68m, result.Total);

            Assert.Equal(2, result.Rentals.Count);

            Assert.Equal("TestTitle1", result.Rentals[0].Title);
            Assert.Equal(1.23m, result.Rentals[0].Cost);

            Assert.Equal("TestTitle2", result.Rentals[1].Title);
            Assert.Equal(4.56m, result.Rentals[1].Cost);

            Assert.Equal(1, result.BuyAndKeep.Count);

            Assert.Equal("TestTitle3", result.BuyAndKeep[0].Title);
            Assert.Equal(7.89m, result.BuyAndKeep[0].Cost);
        }
    }
}
