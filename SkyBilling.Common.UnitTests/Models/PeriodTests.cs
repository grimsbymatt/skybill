﻿using System;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class PeriodTests
    {
        [Fact]
        public void From_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testDateTime = DateTime.Parse("2016-05-13");

            var classUnderTest = new Period
            {
                From = testDateTime
            };

            // Act
            var result = classUnderTest.From;

            // Assert
            Assert.Equal(testDateTime, result);
        }

        [Fact]
        public void To_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testDateTime = DateTime.Parse("2016-05-13");

            var classUnderTest = new Period
            {
                To = testDateTime
            };

            // Act
            var result = classUnderTest.To;

            // Assert
            Assert.Equal(testDateTime, result);
        }
    }
}
