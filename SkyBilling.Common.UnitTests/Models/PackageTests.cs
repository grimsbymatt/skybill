﻿using System.Collections.Generic;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class PackageTests
    {
        [Fact]
        public void Subscriptions_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testSubscriptions = new List<Subscription>
            {
                new Subscription
                {
                    Name = "Subscription1",
                    Cost = 1.23m,
                    Type = SubscriptionType.Tv
                },
                new Subscription
                {
                    Name = "Subscription2",
                    Cost = 4.56m,
                    Type = SubscriptionType.Broadband
                },
                new Subscription
                {
                    Name = "Subscription3",
                    Cost = 7.89m,
                    Type = SubscriptionType.Talk
                }
            };

            var classUnderTest = new Package
            {
                Subscriptions = testSubscriptions
            };

            // Act
            var result = classUnderTest.Subscriptions;

            // Assert
            Assert.Equal(3, result.Count);

            Assert.Equal("Subscription1", result[0].Name);
            Assert.Equal(1.23m, result[0].Cost);
            Assert.Equal(SubscriptionType.Tv, result[0].Type);

            Assert.Equal("Subscription2", result[1].Name);
            Assert.Equal(4.56m, result[1].Cost);
            Assert.Equal(SubscriptionType.Broadband, result[1].Type);

            Assert.Equal("Subscription3", result[2].Name);
            Assert.Equal(7.89m, result[2].Cost);
            Assert.Equal(SubscriptionType.Talk, result[2].Type);
        }

        [Fact]
        public void Total_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new Package
            {
                Total = 123.45m
            };

            // Act
            var result = classUnderTest.Total;

            // Assert
            Assert.Equal(123.45m, result);
        }
    }
}
