﻿using System;
using System.Collections.Generic;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class CallChargesTests
    {
        [Fact]
        public void Calls_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testTimeSpan = TimeSpan.ParseExact("11.35", "mm'.'ss", null);

            var testCalls = new List<Call>
            {
                new Call
                {
                    Called = "0123456789",
                    Cost = 1.23m,
                    Duration = testTimeSpan
                },
                new Call
                {
                    Called = "0987654321",
                    Cost = 4.56m,
                    Duration = testTimeSpan
                }
            };

            var classUnderTest = new CallCharges
            {
                Calls = testCalls
            };

            // Act
            var result = classUnderTest.Calls;

            // Assert
            Assert.Equal(2, result.Count);

            Assert.Equal("0123456789", result[0].Called);
            Assert.Equal(1.23m, result[0].Cost);
            Assert.Equal(testTimeSpan, result[0].Duration);

            Assert.Equal("0987654321", result[1].Called);
            Assert.Equal(4.56m, result[1].Cost);
            Assert.Equal(testTimeSpan, result[1].Duration);
        }

        [Fact]
        public void Total_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new CallCharges
            {
                Total = 123.45m
            };

            // Act
            var result = classUnderTest.Total;

            // Assert
            Assert.Equal(123.45m, result);
        }
    }
}
