﻿using System;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class StatementTests
    {
        [Fact]
        public void Generated_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testDateTime = DateTime.Parse("2016-05-13");

            var classUnderTest = new Statement
            {
                Generated = testDateTime
            };

            // Act
            var result = classUnderTest.Generated;

            // Assert
            Assert.Equal(testDateTime, result);
        }

        [Fact]
        public void Due_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testDateTime = DateTime.Parse("2016-05-13");

            var classUnderTest = new Statement
            {
                Due = testDateTime
            };

            // Act
            var result = classUnderTest.Due;

            // Assert
            Assert.Equal(testDateTime, result);
        }

        [Fact]
        public void Period_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testFromDateTime = DateTime.Parse("2016-04-13");
            var testToDateTime = DateTime.Parse("2016-05-12");

            var classUnderTest = new Statement
            {
                Period = new Period
                {
                    From = testFromDateTime,
                    To = testToDateTime
                }
            };

            // Act
            var result = classUnderTest.Period;

            // Assert
            Assert.Equal(testFromDateTime, result.From);
            Assert.Equal(testToDateTime, result.To);
        }
    }
}
