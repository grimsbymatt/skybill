﻿using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class SubscriptionTests
    {
        [Theory]
        [InlineData(SubscriptionType.Talk)]
        [InlineData(SubscriptionType.Tv)]
        [InlineData(SubscriptionType.Broadband)]
        public void Type_SetProperty_GetReturnsSameValue(SubscriptionType type)
        {
            // Arrange
            var classUnderTest = new Subscription
            {
                Type = type
            };

            // Act
            var result = classUnderTest.Type;

            // Assert
            Assert.Equal(type, result);
        }

        [Fact]
        public void Name_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new Subscription
            {
                Name = "TestName"
            };

            // Act
            var result = classUnderTest.Name;

            // Assert
            Assert.Equal("TestName", result);
        }

        public void Cost_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new Subscription
            {
                Cost = 1.23m
            };

            // Act
            var result = classUnderTest.Cost;

            // Assert
            Assert.Equal(1.23m, result);
        }
    }
}
