﻿using System;
using SkyBilling.Common.Models;
using Xunit;

namespace SkyBilling.Common.UnitTests.Models
{
    public class CallTests
    {
        [Fact]
        public void Called_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new Call
            {
                Called = "0123456789"
            };

            // Act
            var result = classUnderTest.Called;

            // Assert
            Assert.Equal("0123456789", result);
        }

        [Fact]
        public void Duration_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var testTimeSpan = TimeSpan.ParseExact("11.35", "mm'.'ss", null);

            var classUnderTest = new Call
            {
                Duration = testTimeSpan
            };

            // Act
            var result = classUnderTest.Duration;

            // Assert
            Assert.Equal(testTimeSpan, result);
        }

        [Fact]
        public void Cost_SetProperty_GetReturnsSameValue()
        {
            // Arrange
            var classUnderTest = new Call
            {
                Cost = 2.01m
            };

            // Act
            var result = classUnderTest.Cost;

            // Assert
            Assert.Equal(2.01m, result);
        }
    }
}
