﻿using System;
using SkyBilling.Common.Models;
using SkyBilling.Common.WebRequestHelper;
using Xunit;

namespace SkyBilling.Common.UnitTests.WebRequestHelper
{
    public class WebRequestHelperTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void GetJsonResponse_NullBlankOrEmptyResourceString_ThrowsArgumentNullException(string resource)
        {
            // Arrange
            var classUnderTest = new Common.WebRequestHelper.WebRequestHelper();

            // Act // Assert
            Assert.Throws<ArgumentNullException>(
                () => classUnderTest.GetJsonResponse<SkyBillModel>(resource));
        }

        [Fact]
        public void GetJsonResponse_IncompatibleResource_ThrowsWebRequestException()
        {
            // Arrange
            var classUnderTest = new Common.WebRequestHelper.WebRequestHelper();

            // Act // Assert
            Assert.Throws<WebRequestException>(
                () => classUnderTest.GetJsonResponse<WebRequestHelperTests>("http://google.com"));
        }
    }
}
