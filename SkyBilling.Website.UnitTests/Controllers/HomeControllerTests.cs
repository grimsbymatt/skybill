﻿using System.Web.Mvc;
using Xunit;
using NSubstitute;
using SkyBilling.Common.Models;
using SkyBilling.UnitTests.Common;
using SkyBilling.Website.Controllers;
using SkyBilling.WebServiceApi;

namespace SkyBilling.Website.UnitTests.Controllers
{
    public class HomeControllerTests
    {
        [Fact]
        public void Get_HelperReturnsNull_HelperReceivesCallViewReturnedWithNullModel()
        {
            // Arrange
            var fakeWebService = Substitute.For<IWebServiceApi>();

            fakeWebService
                .GetBill()
                .ReturnsForAnyArgs((SkyBillModel) null);

            var classUnderTest = new HomeController(fakeWebService);

            // Act
            var result = classUnderTest.Index() as ViewResult;

            // Assert
            fakeWebService.Received().GetBill();
            Assert.NotNull(result);
            Assert.Null(result.Model);
        }

        [Fact]
        public void Get_HelperReturnsPopulatedModel_HelperReceivesCallViewReturnedWithModelUnchanged()
        {
            // Arrange
            var returnedModel = TestUtils.GetTestModel();

            var fakeWebService = Substitute.For<IWebServiceApi>();

            fakeWebService
                .GetBill()
                .ReturnsForAnyArgs(returnedModel);

            var classUnderTest = new HomeController(fakeWebService);

            // Act
            var result = classUnderTest.Index() as ViewResult;

            // Assert
            fakeWebService.Received().GetBill();
            Assert.NotNull(result);
            Assert.NotNull(result.Model);
            Assert.Equal(typeof(SkyBillModel), result.Model.GetType());
            TestUtils.CompareModels(returnedModel, result.Model as SkyBillModel);
        }
    }
}
