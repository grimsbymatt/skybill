﻿using System;
using SkyBilling.Website.Extensions;
using Xunit;

namespace SkyBilling.Website.UnitTests.Extensions
{
    public class ViewExtensionsTests
    {
        [Fact]
        public void ToViewFormat_ReturnsStringInCorrectFormat()
        {
            // Arrange
            var testDateTime = DateTime.Parse("2016-05-13");

            // Act
            var result = testDateTime.ToViewFormat();

            // Assert
            Assert.Equal("13 May 2016", result);
        }

        [Fact]
        public void ToViewCurrencyFormat_SmallNumber_ReturnsStringInCorrectFormat()
        {
            // Arrange
            var testDecimal = 1.23m;

            // Act
            var result = testDecimal.ToViewCurrencyFormat();

            // Assert
            Assert.Equal("£1.23", result);
        }

        [Fact]
        public void ToViewCurrencyFormat_SmallNumberNeedsRoundingUp_ReturnsStringInCorrectFormat()
        {
            // Arrange
            var testDecimal = 1.235m;

            // Act
            var result = testDecimal.ToViewCurrencyFormat();

            // Assert
            Assert.Equal("£1.24", result);
        }

        [Fact]
        public void ToViewCurrencyFormat_SmallNumberNeedsRoundingDown_ReturnsStringInCorrectFormat()
        {
            // Arrange
            var testDecimal = 1.234m;

            // Act
            var result = testDecimal.ToViewCurrencyFormat();

            // Assert
            Assert.Equal("£1.23", result);
        }

        [Fact]
        public void ToViewCurrencyFormat_LargeNumber_ReturnsStringInCorrectFormat()
        {
            // Arrange
            var testDecimal = 1234567.89m;

            // Act
            var result = testDecimal.ToViewCurrencyFormat();

            // Assert
            Assert.Equal("£1,234,567.89", result);
        }
    }
}
