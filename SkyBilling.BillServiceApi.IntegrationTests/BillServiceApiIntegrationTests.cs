﻿using Ninject;
using SkyBilling.WebService.Dependencies;
using Xunit;

namespace SkyBilling.BillServiceApi.IntegrationTests
{
    public class BillServiceApiIntegrationTests
    {
        [Fact]
        public void GetBill_CallService_ModelReceivedCorrectly()
        {
            // Arrange
            var billService = GetBillService();

            // Act
            var result = billService.GetBill();

            // Assert
            Assert.NotNull(result);
        }

        #region HelperMethods

        private static IBillServiceApi GetBillService()
        {
            var kernel = new StandardKernel();
            kernel.Load(new WebServiceModule());
            return kernel.Get<IBillServiceApi>();
        }

        #endregion
    }
}
