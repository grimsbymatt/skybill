﻿using System;
using System.Collections.Generic;
using SkyBilling.Common.Models;
using Xunit;


namespace SkyBilling.UnitTests.Common
{
    public static class TestUtils
    {
        public static SkyBillModel GetTestModel()
        {
            var testDateTime = DateTime.Parse("2016-05-13");

            var testTimeSpan = TimeSpan.ParseExact("11.35", "mm'.'ss", null);

            return new SkyBillModel
            {
                Statement = new Statement
                {
                    Generated = testDateTime,
                    Due = testDateTime,
                    Period = new Period
                    {
                        From = testDateTime,
                        To = testDateTime
                    }
                },
                CallCharges = new CallCharges
                {
                    Calls = new List<Call>
                    {
                        new Call {Called = "07777777777", Cost = 0.01m, Duration = testTimeSpan},
                        new Call {Called = "07777777777", Cost = 0.01m, Duration = testTimeSpan},
                        new Call {Called = "07777777777", Cost = 0.01m, Duration = testTimeSpan}
                    }

                },
                Package = new Package
                {
                    Subscriptions = new List<Subscription>
                    {
                        new Subscription
                        {
                            Type = SubscriptionType.Tv,
                            Name = "TestTv",
                            Cost = 2.02m
                        },
                        new Subscription
                        {
                            Type = SubscriptionType.Broadband,
                            Name = "TestBroadband",
                            Cost = 3.02m
                        },
                        new Subscription
                        {
                            Type = SubscriptionType.Talk,
                            Name = "TestTalk",
                            Cost = 4.02m
                        },
                    },
                    Total = 9.06m
                },
                SkyStore = new SkyStore
                {
                    BuyAndKeep = new List<SkyStoreTitle>
                    {
                        new SkyStoreTitle
                        {
                            Title = "Title1",
                            Cost = 5.99m
                        },
                        new SkyStoreTitle
                        {
                            Title = "Title2",
                            Cost = 4.99m
                        }
                    },
                    Rentals = new List<SkyStoreTitle>
                    {
                        new SkyStoreTitle
                        {
                            Title = "Title3",
                            Cost = 3.99m
                        },
                        new SkyStoreTitle
                        {
                            Title = "Title4",
                            Cost = 2.99m
                        },
                        new SkyStoreTitle
                        {
                            Title = "Title5",
                            Cost = 1.99m
                        }
                    },
                    Total = 17.96m
                },
                Total = 123.23m
            };
        }

        public static void CompareModels(SkyBillModel modelA, SkyBillModel modelB)
        {
            Assert.Equal(modelA.Statement.Generated, modelB.Statement.Generated);
            Assert.Equal(modelA.Statement.Due, modelB.Statement.Due);
            Assert.Equal(modelA.Statement.Period.From, modelB.Statement.Period.From);
            Assert.Equal(modelA.Statement.Period.To, modelB.Statement.Period.To);
            Assert.Equal(modelA.Package.Total, modelB.Package.Total);
            Assert.Equal(modelA.Package.Subscriptions.Count, modelB.Package.Subscriptions.Count);

            for (var i = 0; i < modelA.Package.Subscriptions.Count; i++)
            {
                Assert.Equal(modelA.Package.Subscriptions[i].Name, modelB.Package.Subscriptions[i].Name);
                Assert.Equal(modelA.Package.Subscriptions[i].Type, modelB.Package.Subscriptions[i].Type);
                Assert.Equal(modelA.Package.Subscriptions[i].Cost, modelB.Package.Subscriptions[i].Cost);
            }

            Assert.Equal(modelA.CallCharges.Total, modelB.CallCharges.Total);
            Assert.Equal(modelA.CallCharges.Calls.Count, modelB.CallCharges.Calls.Count);

            for (var i = 0; i < modelA.CallCharges.Calls.Count; i++)
            {
                Assert.Equal(modelA.CallCharges.Calls[i].Called, modelB.CallCharges.Calls[i].Called);
                Assert.Equal(modelA.CallCharges.Calls[i].Duration, modelB.CallCharges.Calls[i].Duration);
                Assert.Equal(modelA.CallCharges.Calls[i].Cost, modelB.CallCharges.Calls[i].Cost);
            }

            Assert.Equal(modelA.SkyStore.Total, modelB.SkyStore.Total);
            Assert.Equal(modelA.SkyStore.BuyAndKeep.Count, modelB.SkyStore.BuyAndKeep.Count);
            Assert.Equal(modelA.SkyStore.Rentals.Count, modelB.SkyStore.Rentals.Count);

            for (var i = 0; i < modelA.SkyStore.BuyAndKeep.Count; i++)
            {
                Assert.Equal(modelA.SkyStore.BuyAndKeep[i].Title, modelB.SkyStore.BuyAndKeep[i].Title);
                Assert.Equal(modelA.SkyStore.BuyAndKeep[i].Cost, modelB.SkyStore.BuyAndKeep[i].Cost);
            }

            for (var i = 0; i < modelA.SkyStore.Rentals.Count; i++)
            {
                Assert.Equal(modelA.SkyStore.Rentals[i].Title, modelB.SkyStore.Rentals[i].Title);
                Assert.Equal(modelA.SkyStore.Rentals[i].Cost, modelB.SkyStore.Rentals[i].Cost);
            }
        }
    }
}
