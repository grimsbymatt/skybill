﻿namespace SkyBilling.Common.Models
{
    /// <summary>
    /// The types of subscription available.
    /// </summary>
    public enum SubscriptionType
    {
        Tv, Talk, Broadband
    }
}
