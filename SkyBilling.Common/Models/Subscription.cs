﻿namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for a Sky package subscription.
    /// </summary>
    public class Subscription
    {
        /// <summary>
        /// Type of subscription.
        /// </summary>
        public SubscriptionType Type { get; set; }

        /// <summary>
        /// Subscription name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Subscription cost.
        /// </summary>
        public decimal Cost { get; set; }
    }
}
