﻿using System.Collections.Generic;

namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for call charges of a bill.
    /// </summary>
    public class CallCharges
    {
        /// <summary>
        /// List of calls made.
        /// </summary>
        public List<Call> Calls { get; set; }

        /// <summary>
        /// Total cost of calls.
        /// </summary>
        public decimal Total { get; set; }
    }
}
