﻿namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for bill data received from the billing service.
    /// </summary>
    public class SkyBillModel
    {
        /// <summary>
        /// Statement details.
        /// </summary>
        public Statement Statement { get; set; }

        /// <summary>
        /// Total cost of the bill.
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// The customer's Sky package details.
        /// </summary>
        public Package Package { get; set; }

        /// <summary>
        /// Call charge details.
        /// </summary>
        public CallCharges CallCharges { get; set; }

        /// <summary>
        /// Details of purchases from the Sky Stroe.
        /// </summary>
        public SkyStore SkyStore { get; set; }
    }
}
