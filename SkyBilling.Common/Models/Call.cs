﻿using System;

namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for a telephone call listed on a bill.
    /// </summary>
    public class Call
    {
        /// <summary>
        /// The telephone number called.
        /// </summary>
        public string Called { get; set; }

        /// <summary>
        /// The length of the call.
        /// </summary>
        public TimeSpan Duration { get; set; }

        /// <summary>
        /// The cost of the call.
        /// </summary>
        public decimal Cost { get; set; }
    }
}
