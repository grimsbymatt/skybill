﻿using System;

namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for statement details of a bill.
    /// </summary>
    public class Statement
    {
        /// <summary>
        /// The date the bill was generated.
        /// </summary>
        public DateTime Generated { get; set; }

        /// <summary>
        /// The date the bill is due for payment.
        /// </summary>
        public DateTime Due { get; set; }

        /// <summary>
        /// The time period that the bill covers.
        /// </summary>
        public Period Period { get; set; }
    }
}
