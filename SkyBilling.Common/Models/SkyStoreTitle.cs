﻿namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for a purchase from the Sky Store of a bill.
    /// </summary>
    public class SkyStoreTitle
    {
        /// <summary>
        /// Title purchased.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Title cost.
        /// </summary>
        public decimal Cost { get; set; }
    }
}
