﻿using System.Collections.Generic;

namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for Sky package details of a bill.
    /// </summary>
    public class Package
    {
        /// <summary>
        /// Package subscriptions.
        /// </summary>
        public List<Subscription> Subscriptions { get; set; }

        /// <summary>
        /// Total cost of the package.
        /// </summary>
        public decimal Total { get; set; }
    }
}
