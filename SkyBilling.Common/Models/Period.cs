﻿using System;

namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for time period of a bill.
    /// </summary>
    public class Period
    {
        /// <summary>
        /// Start of the time period.
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        /// End of the time period.
        /// </summary>
        public DateTime To { get; set; }
    }
}
