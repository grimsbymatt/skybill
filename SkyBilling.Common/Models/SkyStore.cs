﻿using System.Collections.Generic;

namespace SkyBilling.Common.Models
{
    /// <summary>
    /// Model class for purchases from the Sky Store of a bill.
    /// </summary>
    public class SkyStore
    {
        /// <summary>
        /// List of titles rented.
        /// </summary>
        public List<SkyStoreTitle> Rentals { get; set; }

        /// <summary>
        /// List of titles purchased.
        /// </summary>
        public List<SkyStoreTitle> BuyAndKeep { get; set; }

        /// <summary>
        /// Total cost of purchases.
        /// </summary>
        public decimal Total { get; set; }
    }
}
