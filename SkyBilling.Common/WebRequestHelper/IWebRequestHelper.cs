﻿namespace SkyBilling.Common.WebRequestHelper
{
    public interface IWebRequestHelper
    {
        /// <summary>
        /// Makes an HTTP get request for JSON to the specified resource and 
        /// deserialises the returned JSON to the specified type.  
        /// </summary>
        /// <typeparam name="T">The type to which the returned JSON
        /// should be deserialised.
        /// </typeparam>
        /// <param name="resource">The address of the HTTP resource.</param>
        /// <returns>An object of type <see cref="T"/>.</returns>
        T GetJsonResponse<T>(string resource);
    }
}
