﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace SkyBilling.Common.WebRequestHelper
{
    /// <summary>
    /// Implementation of <see cref="IWebRequestHelper"/>.
    /// </summary>
    internal class WebRequestHelper : IWebRequestHelper
    {
        /// <inheritdoc />
        public T GetJsonResponse<T>(string resource)
        {
            if (string.IsNullOrWhiteSpace(resource))
            {
                throw new ArgumentNullException(nameof(resource));
            }

            var request = WebRequest.CreateHttp(resource);

            request.ContentType = Constants.JsonContentString;

            using (var response = request.GetResponse() as HttpWebResponse)
            {
                if (response == null)
                {
                    throw new WebRequestException(Constants.NoResponseErrorString);
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new WebRequestException(Constants.ServerErrorPrefixString + 
                            $"{response.StatusCode}: {response.StatusDescription}");
                }

                // ReSharper disable once AssignNullToNotNullAttribute
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    try
                    {
                        return JsonConvert.DeserializeObject<T>(streamReader.ReadToEnd());
                    }
                    catch (JsonReaderException)
                    {
                        throw new WebRequestException(Constants.SerialisationExceptionMessage);
                    }
                    
                }
            }
        }
    }
}
