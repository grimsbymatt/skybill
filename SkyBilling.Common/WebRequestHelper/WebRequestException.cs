﻿using System;
using System.Runtime.Serialization;

namespace SkyBilling.Common.WebRequestHelper
{
    /// <summary>
    /// Exception class for errors in making web requests
    /// </summary>
    public class WebRequestException : Exception
    {
        public WebRequestException()
        {
        }

        public WebRequestException(string message) : base(message)
        {
        }

        public WebRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WebRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
