﻿namespace SkyBilling.Common.WebRequestHelper
{
    public static class Constants
    {
        internal const string JsonContentString = "application/json; charset=utf-8";

        internal const string NoResponseErrorString = "Server Error - no response";

        internal const string ServerErrorPrefixString = "Server Error (HTTP ";

        internal const string SerialisationExceptionMessage = 
            "Error deserialising response";
    }
}
