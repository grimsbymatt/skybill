﻿using System;
using Xunit;
using NSubstitute;
using SkyBilling.Common.Models;
using SkyBilling.Common.WebRequestHelper;
using SkyBilling.UnitTests.Common;

namespace SkyBilling.BillServiceApi.UnitTests
{
    public class BillServiceApiTests
    {
        [Fact]
        public void BillServiceApi_PassNullToConstructor_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(
                () => new BillServiceApi(null));
        }

        [Fact]
        public void GetBill_HelperReturnsNull_HelperReceivesCallAndNullIsReturned()
        {
            // Arrange
            var fakeWebRequestHelper = Substitute.For<IWebRequestHelper>();

            fakeWebRequestHelper
                .GetJsonResponse<SkyBillModel>(Arg.Any<string>())
                .ReturnsForAnyArgs((SkyBillModel) null);

            var classUnderTest = new BillServiceApi(fakeWebRequestHelper);

            // Act
            var result = classUnderTest.GetBill();

            // Assert
            fakeWebRequestHelper
                .Received()
                .GetJsonResponse<SkyBillModel>("http://safe-plains-5453.herokuapp.com/bill.json");

            Assert.Null(result);
        }

        [Fact]
        public void GetBill_HelperReturnsPopulatedModel_HelperReceivesCallAndResponseIsReturnedUnchanged()
        {
            // Arrange
            var returnedModel = TestUtils.GetTestModel();

            var fakeWebRequestHelper = Substitute.For<IWebRequestHelper>();

            fakeWebRequestHelper
                .GetJsonResponse<SkyBillModel>(Arg.Any<string>())
                .ReturnsForAnyArgs(returnedModel);

            var classUnderTest = new BillServiceApi(fakeWebRequestHelper);

            // Act
            var result = classUnderTest.GetBill();

            // Assert
            fakeWebRequestHelper
                .Received()
                .GetJsonResponse<SkyBillModel>("http://safe-plains-5453.herokuapp.com/bill.json");
            Assert.NotNull(result);
            TestUtils.CompareModels(returnedModel, result);
        }
    }
}
