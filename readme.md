﻿# SkyBill
---
In this solution of the billing task, the front-end is built in an ASP.NET MVC project. This consumes a web service, which is built in an ASP.NET Web API project, which in turn consumes the existent billing back-end service.

For convenience, all code is contained in a single Visual Studio solution.
### Requirements
---
* Visual Studio 2015 (earlier versions are untested)
* .NET Framework 4.52
* Internet connection

### Instructions
---
* Clone this repository to a local folder
    * git clone https://bitbucket.org/grimsbymatt/skybill.git
* Open ~/skybill/SkyBilling.sln in Visual Studio
* Build the solution (in testing this procedure, repeated builds were occasionally required to eliminate errors)
* Amend the start action of the Web API project SkyBilling.WebService to not open a web page:
    * Right-click SkyBilling.WebService project in Solution Explorer
    * Click 'Properties'
    * Select section 'Web'
    * Select option 'Don't open a page. Wait for a request from an external application.'
* Set the solution to start both the SkyBilling.WebService Web API and the SkyBilling.Website MVC projects on start:
    * Right-click the solution file in Solution Explorer
    * Click 'Properties'
    * In 'Common Properties', select 'Startup Project' section
    * Select 'Multiple startup projects'
    * In the project list below, find SkyBilling.Website and SkyBilling.Webservice and set the action of both to 'Start'
    * Click 'OK'
* Start solution via F5 or the Start button in the toolbar
* The billing page should open in the default browser
Otherwise, if the projects have started, it should be available at http://localhost:51608/