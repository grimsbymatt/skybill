﻿using Ninject.Modules;
using SkyBilling.BillServiceApi;
using SkyBilling.Common.WebRequestHelper;

namespace SkyBilling.WebService.Dependencies
{
    /// <summary>
    /// Ninject configuration for SkyBilling.WebService
    /// </summary>
    public class WebServiceModule : NinjectModule
    {
        /// <inheritdoc />
        public override void Load()
        {
            // SkyBilling.Common
            Bind<IWebRequestHelper>().To<WebRequestHelper>();

            // SkyBilling.BillServiceApi
            Bind<IBillServiceApi>().To<BillServiceApi.BillServiceApi>();
        }
    }
}
