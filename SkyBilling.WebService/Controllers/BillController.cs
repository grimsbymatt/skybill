﻿using System.Web.Http;
using Ninject;
using SkyBilling.BillServiceApi;
using SkyBilling.Common.Models;
using SkyBilling.WebService.Dependencies;

namespace SkyBilling.WebService.Controllers
{
    public class BillController : ApiController
    {
        private IBillServiceApi BillServiceApi { get; }

        public BillController()
        {
            var kernel = new StandardKernel();
            kernel.Load(new WebServiceModule());
            this.BillServiceApi = kernel.Get<IBillServiceApi>();
        }

        /// <summary>
        /// Internal constructor for testing that takes
        /// <see cref="IBillServiceApi"/> dependency.
        /// </summary>
        /// <param name="billServiceApi">Implementation
        /// of <see cref="IBillServiceApi"/>.</param>
        internal BillController(IBillServiceApi billServiceApi)
        {
            this.BillServiceApi = billServiceApi;
        }

        public SkyBillModel Get()
        {
            return this.BillServiceApi.GetBill();
        }
    }
}